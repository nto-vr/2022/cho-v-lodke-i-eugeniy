using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perescope : MonoBehaviour
{
    public Transform gun;
    public Camera camera;

    public Transform battery;
    public Vector3 speedBattery;

    void FixedUpdate()
    {
        gun.transform.localRotation = camera.transform.localRotation;
    }

    public void Shot()
    {
        if (battery == null)
        {
            return;
        }

        battery.transform.position = gun.position;
        battery.transform.localRotation = camera.transform.localRotation;
        Rigidbody rigidbody = battery.GetComponent<Rigidbody>();
        rigidbody.velocity = battery.transform.localRotation * speedBattery;
        rigidbody.angularVelocity = Vector3.zero;
    }
}
