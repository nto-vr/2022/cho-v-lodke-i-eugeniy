using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class LeverLogic : MonoBehaviour
{
    public float leverShiftDelay = 0.5f;
    public float leverShiftElapsed = 0f;
    public bool off = false; 
    public bool offUpdate = true;
    public Transform lightsHolder;
    private LightLogic lightLogic;

    private Vector3 onLocalScale;
    private Vector3 offLocalScale;

    void Start()
    {
        lightLogic = lightsHolder.GetComponent<LightLogic>();

        offLocalScale = transform.localScale;
        onLocalScale = transform.localScale;
        if (off)
        {
            onLocalScale = new Vector3(
                onLocalScale.x,
                onLocalScale.y,
                -onLocalScale.z
            );
        }
        else
        {
            offLocalScale = new Vector3(
                offLocalScale.x,
                offLocalScale.y,
                -offLocalScale.z
            );
        }
    }

    void FixedUpdate()
    {
        if (offUpdate)
        {
            lightLogic.offUpdate = true;
            if (off)
            {
                transform.localScale = offLocalScale;
            }
            else
            {
                transform.localScale = onLocalScale;
            }

            lightLogic.off = off;
            offUpdate = false;
        }
    }

    void HandHoverUpdate(Hand hand)	{
        if (leverShiftElapsed < leverShiftDelay)
        {
            leverShiftElapsed += Time.fixedDeltaTime;
            return;
        }

		if (hand.GetGrabStarting() != GrabTypes.None) {
			offUpdate = true;
            off = !off;
			leverShiftElapsed = 0f;
		}
	}
}
