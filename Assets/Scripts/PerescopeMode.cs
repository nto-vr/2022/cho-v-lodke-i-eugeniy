using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerescopeMode : MonoBehaviour
{
    public GameObject periscopeVRCamera;


    public void SwitchingToPeriscopeMode(Transform battery)
    {
        periscopeVRCamera.SetActive(true);
        periscopeVRCamera.GetComponent<PerescopeController>().battery = battery;
        gameObject.SetActive(false);
    }

    public void SwitchingToNormalMode()
    {
        periscopeVRCamera.SetActive(false);
        gameObject.SetActive(true);
    }

    void Start()
    {
        periscopeVRCamera.SetActive(false);
    }

}
