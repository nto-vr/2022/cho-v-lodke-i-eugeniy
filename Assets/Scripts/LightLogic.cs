using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightLogic : MonoBehaviour
{
    public List<Transform> lights;
    public float offLightsIntervalUp = 120f;
    public float offLightsIntervalBottom = 100f;

    public Transform lever;
    private LeverLogic leverLogicScript;

    public bool off = false;
    public bool offUpdate = true;
    public float offLightsTime = 0f;
    private float offLightsElapsed = 0f;
    private List<float> startLightsIntensity = new List<float>();

    public Transform golodram;


    void Start()
    {
        offLightsTime = Random.Range(offLightsIntervalBottom, offLightsIntervalUp);
        leverLogicScript = lever.GetComponent<LeverLogic>();

        foreach (Transform light in lights)
        {
            startLightsIntensity.Add(light.GetComponent<Light>().intensity);
        }
    }

    void FixedUpdate()
    {
        if (!off)
        {
            if (offLightsElapsed < offLightsTime)
            {
                offLightsElapsed += Time.fixedDeltaTime;
            }
            else
            {
                leverLogicScript.offUpdate = true;
                leverLogicScript.off = true;
            }
        }
        if (offUpdate)
        {
            if (off)
            {
                Off();
            }
            else
            {
                On();
            }
            offUpdate = false;
        }
    }

    void Off()
    {
        off = true;

        foreach (Transform light in lights)
        {
            light.GetComponent<Light>().intensity = 0;
        }

        golodram.gameObject.SetActive(false);
    }

    void On()
    {
        offLightsElapsed = 0;
        offLightsTime = Random.Range(offLightsIntervalBottom, offLightsIntervalUp);
        off = false;

        for (int i = 0; i < lights.Count; i++)
        {
            lights[i].GetComponent<Light>().intensity = startLightsIntensity[i];
        }

        golodram.gameObject.SetActive(true);
    }
}
