using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class SceneLoader : MonoBehaviour
{
    public string entrySceneName;

    public TextMeshProUGUI text;
    public RectTransform fill;

    public float waitBefore = 0.25f;
    public float waitAfter = 0.25f;

    void Start()
    {
        fill.localScale = new Vector3(0, 1, 1);
        text.text = "0%";
        StartCoroutine(LoadSceneAsync());
    }

    void UpdateUI(float operationProgress)
    {
        float progressValue = Mathf.Clamp01(operationProgress) / 0.9f;
        text.text = Mathf.Round(progressValue * 100) + "%";
        fill.localScale = new Vector3(progressValue, 1, 1);
    }

    IEnumerator LoadSceneAsync()
    {
        yield return new WaitForSeconds(waitBefore);

        AsyncOperation loadingOperation = SceneManager.LoadSceneAsync(entrySceneName);
        loadingOperation.allowSceneActivation = false;
        while (loadingOperation.progress < 0.9f)
        {
            UpdateUI(loadingOperation.progress);

            yield return new WaitForEndOfFrame();
        }
        UpdateUI(loadingOperation.progress);

        yield return new WaitForSeconds(waitAfter);

        loadingOperation.allowSceneActivation = true;
    }
}
