using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class SwitchingToPeriscopeMode : MonoBehaviour
{
    public bool keyTurned;
    public Transform battery;

    public Transform player;
    private PerescopeMode periscopeModeScript;

    public Transform lever;
    private GunReload gunReloadScript;

    public Vector3 openRotation;
    public Vector3 closeRotation;

    public bool moving = false;
    public bool movingUp = true;

    public float openingTime = 2f;
    private float elapsed = 0f;

    void Start()
    {
        periscopeModeScript = player.GetComponent<PerescopeMode>();
        gunReloadScript = lever.GetComponent<GunReload>();

        transform.localRotation = Quaternion.Euler(closeRotation);
    }

    void FixedUpdate()
    {
        if (moving)
        {
            if (movingUp)
            {
                if (elapsed < openingTime)
                {
                    transform.localRotation = Quaternion.Euler(closeRotation + (openRotation - closeRotation) * elapsed / openingTime);
                    elapsed += Time.fixedDeltaTime;
                }
                else
                {
                    elapsed = openingTime;
                    movingUp = false;

                    keyTurned = gunReloadScript.gunChange;
                }
            }
            else
            {
                if (elapsed > 0)
                {
                    transform.localRotation = Quaternion.Euler(closeRotation + (openRotation - closeRotation) * elapsed / openingTime);
                    elapsed -= Time.fixedDeltaTime;
                }
                else
                {
                    elapsed = 0;
                    movingUp = true;
                    moving = false;
                }
            }
        }

        if (keyTurned)
        {
            periscopeModeScript.SwitchingToPeriscopeMode(battery);
            keyTurned = false;
        }
        battery = gunReloadScript.battery;
    }

    void HandHoverUpdate(Hand hand)	{
        if (moving)
        {
            return;
        }

		if (hand.GetGrabStarting() != GrabTypes.None) {
			moving = true;
		}
	}
}
