using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hologram : MonoBehaviour
{
    public Transform miniShip;
    public Transform hologramInPlace;
    private Collider hologramCollider;
    public float scale = 100;

    public Transform hologramAsteroidPrefab;
    public Transform hologramDangerousAsteroidPrefab;
    public Camera VRCamera;

    private Dictionary<int, Transform> touchedAsteroids = new Dictionary<int, Transform>();
    private Dictionary<int, Transform> hologramAsteroids = new Dictionary<int, Transform>();

    public static string asteroidTag = "Asteroid";
    public static string dangerousAsteroidTag = "DangerousAsteroid";

    // Returns localPosition
    Vector3 SpaceToHologram(Vector3 inSpace)
    {
        return (inSpace - transform.position) / scale;
    }

    void RegisterAsteroid(Collider collider)
    {
        int id = collider.GetInstanceID();
        if (!touchedAsteroids.ContainsKey(id))
        {
            touchedAsteroids.Add(id, collider.transform);
            Transform asteroid = null;
            if (collider.tag == dangerousAsteroidTag)
            {
                Transform prefab = Instantiate(hologramDangerousAsteroidPrefab, hologramInPlace);
                prefab.localPosition = SpaceToHologram(collider.transform.position);
                asteroid = prefab.GetChild(0);
                Canvas canvas = prefab.GetComponentInChildren<Canvas>();
                canvas.worldCamera = VRCamera;
                TimeController script = canvas.GetComponent<TimeController>();
                script.asteroidLogic = collider.GetComponent<AsteroidLogic>();
            }
            else
            {
                asteroid = Instantiate(hologramAsteroidPrefab, hologramInPlace);
                asteroid.localPosition = SpaceToHologram(collider.transform.position);
            }
            asteroid.rotation = collider.transform.rotation;
            asteroid.localScale = collider.transform.localScale / scale;
            hologramAsteroids.Add(id, asteroid);
        }
    }

    void UnregisterAsteroid(Collider collider)
    {
        int id = collider.GetInstanceID();
        if (touchedAsteroids.ContainsKey(id))
        {
            touchedAsteroids.Remove(id);
            GameObject hologramAsteroid = hologramAsteroids[id].gameObject;
            Destroy(hologramAsteroid);
            hologramAsteroids.Remove(id);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        hologramCollider = hologramInPlace.GetComponent<Collider>();
        transform.localScale = 2 * hologramCollider.bounds.extents * scale;
        Vector3 localPosition = SpaceToHologram(miniShip.position);
        miniShip.parent = hologramInPlace;
        miniShip.localPosition = localPosition;
        miniShip.localScale /= scale;
        miniShip.gameObject.SetActive(true);
    }

    void FixedUpdate()
    {
        foreach (int id in hologramAsteroids.Keys)
        {
            if (touchedAsteroids[id] == null)
            {
                continue;
            }
            if (touchedAsteroids[id].tag == asteroidTag)
                hologramAsteroids[id].localPosition = SpaceToHologram(touchedAsteroids[id].position);
            else
                hologramAsteroids[id].parent.localPosition = SpaceToHologram(touchedAsteroids[id].position);
            hologramAsteroids[id].rotation = touchedAsteroids[id].rotation;
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == asteroidTag || collider.tag == dangerousAsteroidTag)
        {
            RegisterAsteroid(collider);
        }
    }

    void OnTriggerStay(Collider collider)
    {
        if (collider.tag == asteroidTag || collider.tag == dangerousAsteroidTag)
        {
            RegisterAsteroid(collider);
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.tag == asteroidTag || collider.tag == dangerousAsteroidTag)
        {
            UnregisterAsteroid(collider);
        }
    }
}
