using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    private AudioSource audioSource;
    private SoundLevel soundLevel;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        soundLevel = FindObjectOfType<SoundLevel>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        audioSource.volume = soundLevel.value;
    }
}
