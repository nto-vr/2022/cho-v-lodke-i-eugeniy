using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Menu : MonoBehaviour
{
    public TextMeshProUGUI soundLevelText;
    public Slider soundSlider;
    SoundLevel soundLevel;

    void UpdateUI()
    {
        soundLevelText.text = Mathf.Round(soundSlider.value * 100).ToString() + "%";
        soundLevel.value = soundSlider.value;
    }

    void Start()
    {
        soundLevel = FindObjectOfType<SoundLevel>();
        soundSlider.value = soundLevel.value;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateUI();
    }
}
