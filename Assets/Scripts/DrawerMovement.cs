using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class DrawerMovement : MonoBehaviour
{
    public Transform drawer;
    private CheckBattaeryLocation checkBattaeryLocationScript;
    public Transform battery;

    public Vector3 openPosition;
    public Vector3 closePosition;

    public bool open = true;
    public bool moving = false;

    public float openingTime = 2f;
    private float elapsed = 0f;

    public float buttonShiftDelay = 0.5f;
    private float buttonShiftElapsed = 0f;

    public bool batteryInDrawer = false;

    public Material pluggedMaterial;
    public Material notPluggedMaterial;
    void Start()
    {
        checkBattaeryLocationScript = drawer.GetComponent<CheckBattaeryLocation>();

        if (open)
        {
            elapsed = openingTime;
            drawer.localPosition = openPosition;
        }
        else
        {
            elapsed = 0;
            drawer.localPosition = closePosition;
        }
    }

    void FixedUpdate()
    {
        if (buttonShiftElapsed < buttonShiftDelay)
        {
            buttonShiftElapsed += Time.fixedDeltaTime;
        }

        if (!open)
        {
            if (moving)
            {
                if (elapsed < openingTime)
                {
                    drawer.localPosition = closePosition + (openPosition - closePosition) * elapsed / openingTime;
                    elapsed += Time.fixedDeltaTime;
                }
                else
                {
                    elapsed = openingTime;
                    drawer.localPosition = openPosition;
                    moving = false;
                    open = true;
                }
            }
        }
        else
        {
            if (moving)
            {
                if (elapsed > 0)
                {
                    drawer.localPosition = closePosition + (openPosition - closePosition) * elapsed / openingTime;
                    elapsed -= Time.fixedDeltaTime;
                }
                else
                {
                    drawer.localPosition = closePosition;
                    elapsed = 0f;
                    moving = false;
                    open = false;
                }
            }
        }

        batteryInDrawer = !moving && !open && checkBattaeryLocationScript.batteryInDrawer;    
        if (batteryInDrawer)
        {
            GetComponent<Renderer>().material = pluggedMaterial;
            battery = checkBattaeryLocationScript.batteries[0];
        }
        else
        {
            GetComponent<Renderer>().material = notPluggedMaterial;
            battery = null;
        }
    }

    void HandHoverUpdate(Hand hand)	{
        if (buttonShiftElapsed < buttonShiftDelay)
        {
            return;
        }

		if (hand.GetGrabStarting() != GrabTypes.None) {
			moving = true;
			buttonShiftElapsed = 0f;
		}
	}

    
}
