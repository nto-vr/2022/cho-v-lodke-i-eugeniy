using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class TimeController : MonoBehaviour
{
    private Canvas canvas;
    private TextMeshProUGUI text;
    public AsteroidLogic asteroidLogic;

    void Start()
    {
        canvas = GetComponent<Canvas>();
        text = this.GetComponentInChildren<TextMeshProUGUI>();
    }

    void FixedUpdate()
    {
        canvas.transform.LookAt(canvas.worldCamera.transform.position);
        canvas.transform.rotation = canvas.worldCamera.transform.rotation;

        if (asteroidLogic.timeToCollision > 0)
            text.text = Math.Round(asteroidLogic.timeToCollision, 1).ToString() + "s";
        else
            text.text = "";
    }
}
