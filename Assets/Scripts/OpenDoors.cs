using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoors : MonoBehaviour
{
    public Transform leftDoor;
    public Transform rightDoor;

    public float openingTime;
    private float elapsed = 0f;

    public Vector3 leftDoorOpenPosition;
    public Vector3 leftDoorClosePosition;
    public Vector3 rightDoorOpenPosition;
    public Vector3 rightDoorClosePosition;
    
    public bool opened = false;
    public bool moving = false;

    void Start()
    {
        if (opened)
        {
            leftDoor.position = leftDoorOpenPosition;
            rightDoor.position = rightDoorOpenPosition;
        }
        else
        {
            leftDoor.position = leftDoorClosePosition;
            rightDoor.position = rightDoorClosePosition;
        }
    }

    void FixedUpdate()
    {
        if (!opened)
        {
            if (moving)
            {
                if (elapsed < openingTime)
                {
                    leftDoor.position = leftDoorClosePosition + (leftDoorOpenPosition - leftDoorClosePosition) * elapsed / openingTime;
                    rightDoor.position = rightDoorClosePosition + (rightDoorOpenPosition - rightDoorClosePosition) * elapsed / openingTime;
                    elapsed += Time.fixedDeltaTime;
                }
                else
                {
                    elapsed = openingTime;
                    leftDoor.position = leftDoorOpenPosition;
                    rightDoor.position = rightDoorOpenPosition;
                    moving = false;
                    opened = true;
                }
            }
        }
        else
        {
            if (moving)
            {
                if (elapsed > 0)
                {
                    leftDoor.position = leftDoorClosePosition + (leftDoorOpenPosition - leftDoorClosePosition) * elapsed / openingTime;
                    rightDoor.position = rightDoorClosePosition + (rightDoorOpenPosition - rightDoorClosePosition) * elapsed / openingTime;
                    elapsed -= Time.fixedDeltaTime;
                }
                else
                {
                    leftDoor.position = leftDoorClosePosition;
                    rightDoor.position = rightDoorClosePosition;
                    elapsed = 0f;
                    moving = false;
                    opened = false;
                }
            }
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {
            if (!moving) {
                if (!opened) {
                    moving = true;
                } 
            }else {
                if (opened) {
                    opened = false;
                }
            }
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.tag == "Player") {
            if (!moving) {
                if (opened) {
                    moving = true;
                }
            }else {
                if (!opened) {
                    opened = true;
                }
            }
        }
    }
}