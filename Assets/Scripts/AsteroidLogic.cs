using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidLogic : MonoBehaviour
{
    public float timeToCollision;


    void FixedUpdate()
    {
        timeToCollision -= Time.fixedDeltaTime;
    }

    void OnTriggerEnter(Collider other)
    {
        print(other.tag);
        if (other.tag == "SpaceShip" || other.tag == "Battery")
        {
            Destroy(gameObject);
        }
    }
}
