using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidDestoyer : MonoBehaviour
{
    public string asteroidTag;
    public float asteroidZoneRadius = 100f;
    
    void FixedUpdate()
    {
        GameObject[] asteroids = GameObject.FindGameObjectsWithTag(asteroidTag);

        foreach (GameObject asteroid in asteroids)
        {
            float dist = Vector3.Distance(asteroid.transform.position, transform.position);
            if (dist > asteroidZoneRadius) {
                Destroy(asteroid);
            }
        }
    }
}
