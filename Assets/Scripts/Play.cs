using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Play : MonoBehaviour
{
    private Button button;
    public Transform menu;
    public Transform loadingScreen;

    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(PlayAction);
    }

    // Update is called once per frame
    void PlayAction()
    {
        menu.gameObject.SetActive(false);
        loadingScreen.gameObject.SetActive(true);
    }
}
