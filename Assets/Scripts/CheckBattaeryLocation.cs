using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckBattaeryLocation : MonoBehaviour
{
    public bool batteryInDrawer = false;
    public List<Transform> batteries;
    private int batteryCount = 0;

    void OnTriggerEnter(Collider other) {
        if (other.tag == "Battery") {
            batteryCount += 1;
            batteries.Add(other.transform);
            batteryInDrawer = true;
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.tag == "Battery") {
            batteryCount -= 1;
            batteries.Remove(other.transform);
            if (batteryCount == 0)
            {
                batteryInDrawer = false;
            }
        }
    } 
}
