using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidsSpwner : MonoBehaviour
{
    //spawn parametrs
    public BoxCollider spawnAsteroidsZone;
    public Collider spawnDangerousAsteroidsZone;
    public Transform ateriodPrefab;
    public float spawnDelay = 5f;
	private float spawnElapsed = 0f;
    public string dangerousAsteroidTag = "DangerousAsteroid";

    //move parametrs
    [Range(1f, 100f)]
    public float asteroidMassUp = 1f;
    [Range(1f, 100f)]
    public float asteroidMassBottom = 10f;
    public float asteroidEnergy = 100f;
    public Vector3 asteroidVelocityVector = new Vector3(0, 0, 1);

    //
    public Transform starship;

    public static Vector3 RandomPointInBounds(Bounds bounds)
    {
		return new Vector3
        (
			Random.Range(bounds.min.x, bounds.max.x),
			Random.Range(bounds.min.y, bounds.max.y),
			Random.Range(bounds.min.z, bounds.max.z)
		);
	}

    void FixedUpdate()
    {
        //spawninig asteroids from prefab
        SpawnLogic();
    }

    void SpawnLogic()
    {
        if (spawnElapsed < spawnDelay)
        {
			spawnElapsed += Time.fixedDeltaTime;
		}
        else
        {
			spawnElapsed = 0;
			Spawn();
		}
    }

    void Spawn()
    {
        Transform asteroid = Instantiate(ateriodPrefab, RandomPointInBounds(spawnAsteroidsZone.bounds), Quaternion.identity, transform);

        Rigidbody asteroidRigidbody = asteroid.GetComponent<Rigidbody>();
        asteroidRigidbody.mass = Random.Range(asteroidMassUp, asteroidMassBottom);
        
        AsteroidLogic asteroidLogic = asteroid.GetComponent<AsteroidLogic>();
        
        Vector3 directionDeviation = new Vector3
        (
            Random.Range(-0.1f, 0.1f),
            Random.Range(-0.1f, 0.1f),
            Random.Range(-0.1f, 0.1f)
        );
        directionDeviation = Vector3.Normalize(directionDeviation) * 0.1f;
        asteroidRigidbody.velocity = Vector3.Normalize(asteroidVelocityVector + directionDeviation) * Mathf.Sqrt(2f * asteroidEnergy / asteroidRigidbody.mass);
        

        Vector3 asteroidScale = asteroid.transform.localScale;
        if (Random.Range(0f, 1f) < 0.1f)
        {
            asteroidScale = new Vector3
            (
                asteroidScale.x * Random.Range(3f, 4f),
                asteroidScale.y * Random.Range(0.75f, 1f),
                asteroidScale.z * Random.Range(0.75f, 1f)
            );
        }
        else if (Random.Range(0f, 1f) < 0.3f)
        {
            asteroidScale = new Vector3
            (
                asteroidScale.x * Random.Range(1.5f, 2f),
                asteroidScale.y * Random.Range(1.5f, 2f),
                asteroidScale.z * Random.Range(0.75f, 1f)
            );
        }
        else 
        {
            asteroidScale = new Vector3
            (
                asteroidScale.x * Random.Range(0.75f, 1.5f),
                asteroidScale.y * Random.Range(0.75f, 1.5f),
                asteroidScale.z * Random.Range(0.75f, 1.5f)
            );
        }
        asteroid.transform.localScale = asteroidScale;

        Vector3 asteroidVectorRotation = Vector3.Normalize(new Vector3
        (
            Random.Range(-1f, 1f),
            Random.Range(-1f, 1f),
            Random.Range(-1f, 1f)
        ));

        asteroidRigidbody.angularVelocity = asteroidVectorRotation;
        

        if (spawnDangerousAsteroidsZone.ClosestPoint(asteroid.transform.position) == asteroid.transform.position)
        {
            asteroid.tag = dangerousAsteroidTag;
            asteroidLogic.timeToCollision = Vector3.Distance(asteroid.transform.position, starship.transform.position) / asteroidRigidbody.velocity.magnitude;
        }
    }
}
