using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class PerescopeController : MonoBehaviour
{
    public GameObject player;
    private PerescopeMode perescopeModeScript;
    private Perescope perescopeScript;

    public Transform battery;

	public SteamVR_Action_Boolean trigger;
	public SteamVR_Action_Boolean buttonB;


    void Start()
    {
        perescopeModeScript = player.GetComponent<PerescopeMode>();
        perescopeScript = GetComponent<Perescope>();
    }

    void FixedUpdate()
    {
        if (buttonB.stateDown)
        {
            perescopeModeScript.SwitchingToNormalMode();
        }

        if (trigger.state)
        {
            perescopeScript.battery = battery;
            perescopeScript.Shot();
            perescopeScript.battery = null;
            battery = null;
        }
    }
}
