using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class PlayerController : MonoBehaviour
{
    //SteamVR Actions
	public SteamVR_Action_Vector2 touchpadInput;
	
    //player parametrs
	public Transform cameraTransform;
	private CapsuleCollider capsuleCollider;
	private Rigidbody bodyRigidbody;

    //pyblic user settings
	public float height = 2f;
	public float movementVelocity = 1.5f;
	public float movementUpVelocity = 0.5f;

	//умничка

	void Start()
	{
		capsuleCollider = GetComponent<CapsuleCollider>();
		bodyRigidbody = GetComponent<Rigidbody>();
	}

	void FixedUpdate()
	{
    	SmoothMovement();
	}

    //moving player with use touchpad
    void SmoothMovement() {
        //move player
		Vector3 movementDir = Player.instance.hmdTransform.TransformDirection(new Vector3(touchpadInput.axis.x, 0, touchpadInput.axis.y));
		transform.position += Vector3.ProjectOnPlane(Time.fixedDeltaTime * movementDir * movementVelocity, Vector3.up);

        //move player collider
		float distanceFromFloor = Vector3.Dot(cameraTransform.localPosition, Vector3.up);
		capsuleCollider.height = Mathf.Clamp(distanceFromFloor, capsuleCollider.radius, height);
		capsuleCollider.center = cameraTransform.localPosition - 0.5f * Mathf.Min(height, distanceFromFloor) * Vector3.up;
    }

	void OnTriggerStay(Collider other) {
        if (other.tag == "Staits") {
			bodyRigidbody.useGravity = false;
            transform.position += movementUpVelocity * Time.fixedDeltaTime * Vector3.up;
        }
    }

	void OnTriggerExit(Collider other) {
        if (other.tag == "Staits") {
			bodyRigidbody.useGravity = true;
        }
    }
}
