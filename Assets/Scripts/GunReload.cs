using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class GunReload : MonoBehaviour
{
    public bool gunChange = false;
    public bool leverPressed = false;
    public Transform button;
    private DrawerMovement drawerMovementScript;

    public Transform battery;

    public Vector3 openRotation;
    public Vector3 closeRotation;

    public bool moving = false;
    public bool movingUp = true;

    public float openingTime = 2f;
    private float elapsed = 0f;

    public Transform tablePanel;
    public Material pluggedMaterial;
    public Material notPluggedMaterial;
    private Renderer renderer;

    void Start()
    {
        renderer = tablePanel.GetComponent<Renderer>();
        drawerMovementScript = button.GetComponent<DrawerMovement>();

        transform.localRotation = Quaternion.Euler(closeRotation);
    }

    void FixedUpdate()
    {
        if (moving)
        {
            if (movingUp)
            {
                if (elapsed < openingTime)
                {
                    transform.localRotation = Quaternion.Euler(closeRotation + (openRotation - closeRotation) * elapsed / openingTime);
                    elapsed += Time.fixedDeltaTime;
                }
                else
                {
                    elapsed = openingTime;
                    movingUp = false;

                    leverPressed = drawerMovementScript.batteryInDrawer;
                }
            }
            else
            {
                if (elapsed > 0)
                {
                    transform.localRotation = Quaternion.Euler(closeRotation + (openRotation - closeRotation) * elapsed / openingTime);
                    elapsed -= Time.fixedDeltaTime;
                }
                else
                {
                    elapsed = 0;
                    movingUp = true;
                    moving = false;
                }
            }
        }

        gunChange = leverPressed && drawerMovementScript.batteryInDrawer;
        Material[] materials = renderer.materials;
        if (!gunChange)
        {
            materials[1] = notPluggedMaterial;
            leverPressed = false;
        }
        else
        {
            materials[1] = pluggedMaterial;
        }
        renderer.materials = materials;
        battery = drawerMovementScript.battery;
    }

    void HandHoverUpdate(Hand hand)	{
        if (moving)
        {
            return;
        }

		if (hand.GetGrabStarting() != GrabTypes.None) {
			moving = true;
		}
	}
}
